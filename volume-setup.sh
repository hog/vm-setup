#!/usr/bin/env bash
OLD_DIR=`pwd`
THIS_DIR="$(dirname "$0")"


if [ -e /dev/vdb ]; then
    echo "[Hog VM Setup] Formatting and mounting /dev/vdb..."
    mkfs.ext4 /dev/vdb
    mkdir /mnt/vd
    chmod a+xrw /mnt/vd
    mount /dev/vdb /mnt/vd
else
	echo
    echo "[Hog VM Setup] WARINING /dev/vdb not found. Please attach a volume to the VM on openstack.cern.ch, before running this script"
    exit -1
fi

echo
echo "[Hog VM Setup] Creating swap file, this might take a while..."
dd if=/dev/zero of=/swapfile bs=1024 count=16777216
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile

