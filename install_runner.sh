#!/usr/bin/env bash

function help_message() {
  echo
  echo " Hog - Install gitlab runner"
  echo " ---------------------------"
  echo " Install the Gitlab Runner on a linux machine"
  echo
  echo " Usage: $1 -u <user_name> -g <user_group> -t <gitlab_token> [OPTIONS]"
  echo " Options: --tags: <list of tags for this VM>, default: hog,vivado"
  echo "          --url: <Gitlab url>. Default: https://gitlab.cern.ch"
  echo "          --output_limit: <maximum size of output log in the CI jobs. Default: 0 (no limit)>"
  echo
}




OLD_DIR=`pwd`
THIS_DIR="$(dirname "$0")"

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

if [ "$1" == "-h" ] || [ "$1" == "-help" ] || [ "$1" == "--help" ] || [ "$1" == "-H" ]; then
    help_message $0
    exit 0
fi

case $key in
    -u|--user)
    USER="$2"
    shift # past argument
    shift # past value
    ;;
    -g|--group)
    GROUP="$2"
    shift # past argument
    shift # past value
    ;;
    -t|--token)
    TOKEN="$2"
    shift # past argument
    shift # past value
    ;;
    --tags)
	TAGS="$2"
	shift # past argument
    shift # past value
    ;;
    --url)
	URL="$2"
	shift # past argument
    shift # past value
    ;;
    --output_limit)
    OUTPUT_LIMIT=$2
    shift # past argument
    shift # past value
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters


if [ "$(whoami)" != "root" ]; then
    echo "[Hog VM Setup] FATAL: Script must be run as root."
    exit -1
fi

if [ -v USER ]; then
    echo "[Hog VM Setup] The user Hog will use is: $USER"
else
    echo "[Hog VM Setup] ERROR: variable USER should be set with a valid CERN user name"
    help_message $0
    exit -1
fi

if [ -v GROUP ]; then
    echo "[Hog VM Setup] The user group Hog will use is: $GROUP"
else
    echo "[Hog VM Setup] ERROR: variable GROUP should be set with a valid CERN user group"
    help_message $0
    exit -1
fi

if [ -v TOKEN ]; then
    echo "[Hog VM Setup] The private token for gitlab acces is set to : $TOKEN"
else
    echo "[Hog VM Setup] ERROR: variable TOKEN should be set with a valid gitlab private token"
    help_message $0
    exit -1
fi

if [ -v TAGS ]; then
    echo "[Hog VM Setup] The tags associated to the gitlab runner on this VM are: $TAGS"
else
    echo "[Hog VM Setup] No tags have been specified. Assigning default tags (hogs,vivado) to the gitlab runner on this Virtual Machine"
    TAGS="hog,vivado"
fi

if [ -v URL ]; then
    echo "[Hog VM Setup] The URL associated to the gitlab runner on this VM is: $URL"
else
    echo "[Hog VM Setup] No URL has been specified. Assigning default URL (https://gitlab.cern.ch) to the gitlab runner on this Virtual Machine"
    URL="https://gitlab.cern.ch"
fi

if [ -v OUTPUT_LIMIT ]; then
    echo "[Hog VM Setup] The log output limit for the Hog-CI will be $OUTPUT_LIMIT byte"
else
    echo "[Hog VM Setup] No $OUTPUT_LIMIT has been specified. Assigning default $OUTPUT_LIMIT (0) to the gitlab runner on this Virtual Machine"
    OUTPUT_LIMIT=0
fi


echo
echo "[Hog VM Setup] Adding $USER user..."
addusercern $USER
echo
echo "[Hog VM Setup] Adding $USER to systemd_journal group..."
usermod -a -G systemd-journal $USER
echo
echo "[Hog VM Setup] Making $USER home..."
mkdir /home/$USER
chmod a+rxw /home/$USER
chown $USER:$GROUP /home/$USER
/sbin/usermod -m -d /home/$USER $USER

echo "[Hog VM Setup] Copying files to $USER home..."
cp -f ./ProfileSetup/hog_bash_profile /home/$USER/.bash_profile
cp -f ./ProfileSetup/hog_bashrc /home/$USER/.bashrc
chown $USER:$GROUP /home/$USER/.bash_profile
chown $USER:$GROUP /home/$USER/.bashrc

echo "[Hog VM Setup] Setting up Gitlab runner..."
gitlab-runner uninstall
gitlab-runner install --user=$USER 
gitlab-runner register \
    --non-interactive \
    --url "$URL" \
    --registration-token "$TOKEN" \
    --executor "shell" \
    --description "Hog Vivado runner on $HOSTNAME" \
    --tag-list "$TAGS" \
    --run-untagged="false" \
    --locked="false" \
    --output-limit $OUTPUT_LIMIT 
gitlab-runner start
